// Charge Account Validation - ProLab10 Program 1.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Program 1 - Charge Account Validation

#include "pch.h"
#include <iostream>

using namespace std;


int accounts[] = { 5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 8080152, 4562555, 5552012, 5050552, 7825877, 1250255, 1005231, 654231, 3852085, 7576651, 7881200, 4581002 };
const int LIST_COUNT = sizeof(accounts);



int getUserInput()
{
	int userInput;

	cout << "Enter your charge account number: " << endl;
	cin >> userInput;

	return userInput;
}

bool searchAccounts(int input)
{
	for (int i = 0; i <= LIST_COUNT; i++)
	{
		if (input == accounts[i])
			return true;
	}
	return false;
}


int main()
{
	int input = getUserInput();

	if (searchAccounts(input) == true)
		cout << "The account number " << input << " is valid." << endl;
	else if (searchAccounts(input) == false)
		cout << "The acount number " << input << " is invalid." << endl;

	return 0;
}