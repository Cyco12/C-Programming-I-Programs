// Grade Book Modification - ProLab9B Program 13.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Program 13 – Grade Book Modification

#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

string studentNames[6]; // stores all our student names
char studentLetterGrades[5]; // stores the letter grades of all our students
double studentTestScoreOne[4], // stores all 4 test scores for student one
studentTestScoreTwo[4], // stores all 4 test scores for student two
studentTestScoreThree[4], // stores all 4 test scores for student three
studentTestScoreFour[4], // stores all 4 test scores for student four
studentTestScoreFive[4], // stores all 4 test scores for student five
studentAverages[5], // stores all our students' averages
studentLowestGrades[4]; // stores the lowest grade of each student


void inputStudentNames(string names[]) // takes input for 5 student names and stores them in the studentNames[] array
{
	for (int i = 0; i < 5; i++)
	{
		cout << "What is student number " << i + 1 << "'s name?\n";
		cin >> studentNames[i + 1];
	}
}

void inputTestScores() // takes input from the user for 4 test scores for each of the 5 students
{
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << "Input " << studentNames[i + 1] << "'s " << j + 1 << " test score\n";
			if (i == 0)
			{
				cin >> studentTestScoreOne[j];
				while (studentTestScoreOne[j] > 100 || studentTestScoreOne[j] < 0) // input validation
				{
					cout << "Invalid input." << " Input a value between 0 and 100" << endl;
					cin >> studentTestScoreOne[j];
				}
			}
			if (i == 1)
			{
				cin >> studentTestScoreTwo[j];
				while (studentTestScoreTwo[j] > 100 || studentTestScoreTwo[j] < 0) // input validation
				{
					cout << "Invalid input." << " Input a value between 0 and 100" << endl;
					cin >> studentTestScoreTwo[j];
				}
			}
			if (i == 2)
			{
				cin >> studentTestScoreThree[j];
				while (studentTestScoreThree[j] > 100 || studentTestScoreThree[j] < 0) // input validation
				{
					cout << "Invalid input." << " Input a value between 0 and 100" << endl;
					cin >> studentTestScoreThree[j];
				}
			}
			if (i == 3)
			{
				cin >> studentTestScoreFour[j];
				while (studentTestScoreFour[j] > 100 || studentTestScoreFour[j] < 0) // input validation
				{
					cout << "Invalid input." << " Input a value between 0 and 100" << endl;
					cin >> studentTestScoreFour[j];
				}
			}
			if (i == 4)
			{
				cin >> studentTestScoreFive[j];
				while (studentTestScoreFive[j] > 100 || studentTestScoreFive[j] < 0) // input validation
				{
					cout << "Invalid input." << " Input a value between 0 and 100" << endl;
					cin >> studentTestScoreFive[j];
				}
			}
		}
	}
}

void findLowest()
{
	const int SIZE = 4;
	double lowest[5] = { 100, 100, 100, 100, 100 };

	for (int i = 0; i <= SIZE - 1; i++)
	{
		// find lowest score for the first student
		if (studentTestScoreOne[i] <= lowest[0])
		{
			lowest[0] = studentTestScoreOne[i];
			studentLowestGrades[0] = lowest[0]; // save the lowest score in our global array for lowest student grades at the first position
		}


		// find lowest score for the second student
		if (studentTestScoreTwo[i] <= lowest[1])
		{
			lowest[1] = studentTestScoreTwo[i];
			studentLowestGrades[1] = lowest[1]; // save the lowest score in our global array for lowest student grades at the second position
		}


		// find lowest score for the third student
		if (studentTestScoreThree[i] <= lowest[2])
		{
			lowest[2] = studentTestScoreThree[i];
			studentLowestGrades[2] = lowest[2]; // save the lowest score in our global array for lowest student grades at the third position
		}


		// find lowest score for the fourth student
		if (studentTestScoreFour[i] <= lowest[3])
		{
			lowest[3] = studentTestScoreFour[i];
			studentLowestGrades[3] = lowest[3]; // save the lowest score in our global array for lowest student grades at the fourth position
		}


		// find lowest score for the fifth student
		if (studentTestScoreFive[i] <= lowest[4])
		{
			lowest[4] = studentTestScoreFive[i];
			studentLowestGrades[4] = lowest[4]; // save the lowest score in our global array for lowest student grades at the fifth position
		}
	}
}

void calculateAverages() // calculates the average grade of all 5 students
{
	double studentScoresSum[5]{ 0, 0, 0, 0, 0 };

	for (int i = 0; i < 4; i++)
	{
		if (studentLowestGrades[0] != studentTestScoreOne[i] || (studentTestScoreOne[i] == studentLowestGrades[0] && i < 3) && studentTestScoreOne[i] == studentTestScoreOne[i + 1])
			studentScoresSum[0] += studentTestScoreOne[i];
		if (studentLowestGrades[1] != studentTestScoreTwo[i] || (studentTestScoreTwo[i] == studentLowestGrades[1] && i < 3) && studentTestScoreTwo[i] == studentTestScoreTwo[i + 1])
			studentScoresSum[1] += studentTestScoreTwo[i];
		if (studentLowestGrades[2] != studentTestScoreThree[i] || (studentTestScoreThree[i] == studentLowestGrades[2] && i < 3) && studentTestScoreThree[i] == studentTestScoreThree[i + 1])
			studentScoresSum[2] += studentTestScoreThree[i];
		if (studentLowestGrades[3] != studentTestScoreFour[i] || (studentTestScoreFour[i] == studentLowestGrades[3] && i < 3) && studentTestScoreFour[i] == studentTestScoreFour[i + 1])
			studentScoresSum[3] += studentTestScoreFour[i];
		if (studentLowestGrades[4] != studentTestScoreFive[i] || (studentTestScoreFive[i] == studentLowestGrades[4] && i < 3) && studentTestScoreFive[i] == studentTestScoreFive[i + 1])
			studentScoresSum[4] += studentTestScoreFive[i];
	}

	studentAverages[0] = studentScoresSum[0] / 3;
	studentAverages[1] = studentScoresSum[1] / 3;
	studentAverages[2] = studentScoresSum[2] / 3;
	studentAverages[3] = studentScoresSum[3] / 3;
	studentAverages[4] = studentScoresSum[4] / 3;
}

char calculateLetterGrade(double avg) // accept a double variable and calculates the letter grade based off of it
{
	char letter;

	if (avg >= 90)
		letter = 'A';
	if (avg >= 80 && avg <= 89)
		letter = 'B';
	if (avg >= 70 && avg <= 79)
		letter = 'C';
	if (avg >= 60 && avg <= 69)
		letter = 'D';
	if (avg <= 59)
		letter = 'F';

	return letter;
}

void calculateLetters() // calculate the letter grades of all 5 students using their averages
{
	studentLetterGrades[0] = calculateLetterGrade(studentAverages[0]);
	studentLetterGrades[1] = calculateLetterGrade(studentAverages[1]);
	studentLetterGrades[2] = calculateLetterGrade(studentAverages[2]);
	studentLetterGrades[3] = calculateLetterGrade(studentAverages[3]);
	studentLetterGrades[4] = calculateLetterGrade(studentAverages[4]);

}

void displayTestScores() // display the averages and the letter grades of all 5 students
{
	cout << studentNames[1] << "'s average is " << studentAverages[0] << " and their letter grade is " << studentLetterGrades[0] << endl;
	cout << studentNames[2] << "'s average is " << studentAverages[1] << " and their letter grade is " << studentLetterGrades[1] << endl;
	cout << studentNames[3] << "'s average is " << studentAverages[2] << " and their letter grade is " << studentLetterGrades[2] << endl;
	cout << studentNames[4] << "'s average is " << studentAverages[3] << " and their letter grade is " << studentLetterGrades[3] << endl;
	cout << studentNames[5] << "'s average is " << studentAverages[4] << " and their letter grade is " << studentLetterGrades[4] << endl;
}


int main() // runs all of our above functions to take in student names, student test scores, calculate average grades, calculate letter grades, then display everything
{
	inputStudentNames(studentNames);
	inputTestScores();
	findLowest();
	calculateAverages();
	calculateLetters();
	displayTestScores();

	return 0;
}