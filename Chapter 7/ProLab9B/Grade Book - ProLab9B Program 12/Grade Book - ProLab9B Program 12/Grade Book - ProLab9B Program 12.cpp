// Grade Book - ProLab9B Program 12.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Lab_Pro_9B.cpp : Defines the entry point for the console application.
// Program 12 - Grade Book

#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

string studentNames[5]; // stores all our student names
char studentLetterGrades[5]; // stores the letter grades of all our students
double studentTestScoreOne[4], // stores all 4 test scores for student one
studentTestScoreTwo[4], // stores all 4 test scores for student two
studentTestScoreThree[4], // stores all 4 test scores for student three
studentTestScoreFour[4], // stores all 4 test scores for student four
studentTestScoreFive[4], // stores all 4 test scores for student five
studentAverages[5]; // stores all our students' averages


void inputStudentNames(string names[]) // takes input for 5 student names and stores them in the studentNames[] array
{
	for (int i = 0; i < 5; i++)
	{
		cout << "What is student number " << i + 1 << "'s name?\n";
		cin >> studentNames[i];
	}
}

void inputTestScores() // takes input from the user for 4 test scores for each of the 5 students
{
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << "Input " << studentNames[i] << "'s " << j + 1 << " test score\n";
			if (i == 0)
			{
				cin >> studentTestScoreOne[j];
				while (studentTestScoreOne[j] > 100 || studentTestScoreOne[j] < 0) // input validation
				{
					cout << "Invalid input." << " Input a value between 0 and 100" << endl;
					cin >> studentTestScoreOne[j];
				}
			}
			if (i == 1)
			{
				cin >> studentTestScoreTwo[j];
				while (studentTestScoreTwo[j] > 100 || studentTestScoreTwo[j] < 0) // input validation
				{
					cout << "Invalid input." << " Input a value between 0 and 100" << endl;
					cin >> studentTestScoreTwo[j];
				}
			}
			if (i == 2)
			{
				cin >> studentTestScoreThree[j];
				while (studentTestScoreThree[j] > 100 || studentTestScoreThree[j] < 0) // input validation
				{
					cout << "Invalid input." << " Input a value between 0 and 100" << endl;
					cin >> studentTestScoreThree[j];
				}
			}
			if (i == 3)
			{
				cin >> studentTestScoreFour[j];
				while (studentTestScoreFour[j] > 100 || studentTestScoreFour[j] < 0) // input validation
				{
					cout << "Invalid input." << " Input a value between 0 and 100" << endl;
					cin >> studentTestScoreFour[j];
				}
			}
			if (i == 4)
			{
				cin >> studentTestScoreFive[j];
				while (studentTestScoreFive[j] > 100 || studentTestScoreFive[j] < 0) // input validation
				{
					cout << "Invalid input." << " Input a value between 0 and 100" << endl;
					cin >> studentTestScoreFive[j];
				}
			}
		}
	}
}

void calculateAverages() // calculates the average grade of all 5 students
{
	double studentScoresSum[5]{ 0, 0, 0, 0, 0 };

	for (int i = 0; i < 4; i++)
	{
		studentScoresSum[0] += studentTestScoreOne[i];
		studentScoresSum[1] += studentTestScoreTwo[i];
		studentScoresSum[2] += studentTestScoreThree[i];
		studentScoresSum[3] += studentTestScoreFour[i];
		studentScoresSum[4] += studentTestScoreFive[i];
	}

	studentAverages[0] = studentScoresSum[0] / 4;
	studentAverages[1] = studentScoresSum[1] / 4;
	studentAverages[2] = studentScoresSum[2] / 4;
	studentAverages[3] = studentScoresSum[3] / 4;
	studentAverages[4] = studentScoresSum[4] / 4;
}

char calculateLetterGrade(double avg) // accept a double variable and calculates the letter grade based off of it
{
	char letter;

	if (avg >= 90)
		letter = 'A';
	if (avg >= 80 && avg <= 89)
		letter = 'B';
	if (avg >= 70 && avg <= 79)
		letter = 'C';
	if (avg >= 60 && avg <= 69)
		letter = 'D';
	if (avg <= 59)
		letter = 'F';

	return letter;
}

void calculateLetters() // calculate the letter grades of all 5 students using their averages
{
	studentLetterGrades[0] = calculateLetterGrade(studentAverages[0]);
	studentLetterGrades[1] = calculateLetterGrade(studentAverages[1]);
	studentLetterGrades[2] = calculateLetterGrade(studentAverages[2]);
	studentLetterGrades[3] = calculateLetterGrade(studentAverages[3]);
	studentLetterGrades[4] = calculateLetterGrade(studentAverages[4]);

}

void displayTestScores() // display the averages and the letter grades of all 5 students
{
	cout << studentNames[0] << "'s average is " << studentAverages[0] << " and their letter grade is " << studentLetterGrades[0] << endl;
	cout << studentNames[1] << "'s average is " << studentAverages[1] << " and their letter grade is " << studentLetterGrades[1] << endl;
	cout << studentNames[2] << "'s average is " << studentAverages[2] << " and their letter grade is " << studentLetterGrades[2] << endl;
	cout << studentNames[3] << "'s average is " << studentAverages[3] << " and their letter grade is " << studentLetterGrades[3] << endl;
	cout << studentNames[4] << "'s average is " << studentAverages[4] << " and their letter grade is " << studentLetterGrades[4] << endl;
}


int main() // runs all of our above functions to take in student names, student test scores, calculate average grades, calculate letter grades, then display everything
{
	inputStudentNames(studentNames);
	inputTestScores();
	calculateAverages();
	calculateLetters();
	displayTestScores();

	return 0;
}