// Chips and Salsa - ProLab9 Program 3.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Project Lab Lesson 9 - Program 3

#include "pch.h"
#include <iostream>
#include <string>
#include <algorithm>

using namespace std;


string salsaNames[] = { "Mild", "Medium", "Sweet", "Hot", "Zesty" }; // holds all our salsa names as strings
int jarsSold[] = { 0, 0, 0, 0, 0 }; // holds the amount of jars sold for all the salsa types; the jars sold are stored in the same order as the string array above: mild, medium, sweet, hot, zesty
int totalJarsSold, highestPos = 0, lowestPos = 0; // holds our total jars sold, highest selling array position, and lowest selling array position

void getInput() // outputs a cout prompting the user for data then reads in all 5 salsa types jars sold in one cin statement
{
	cout << "Please enter the values for the amount of jars sold for the following Salsa types: " << salsaNames[0] << ", " << salsaNames[1] << ", "
		<< salsaNames[2] << ", " << salsaNames[3] << ", and " << salsaNames[4] << ". (Please note the amount of jars sold must be a non-negative integer\n";
	cin >> jarsSold[0] >> jarsSold[1] >> jarsSold[2] >> jarsSold[3] >> jarsSold[4];
}

bool checkInput(int array[], int size) // validates input by checking to make sure the input isnt less than 0
{
	for (int i = 0; i < size; i++)
	{
		if (array[i] < 0)
			return false;
	}
	totalJarsSold = jarsSold[0] + jarsSold[1] + jarsSold[2] + jarsSold[3] + jarsSold[4];
	return true;
}

int getHighest(int array[], int size) // method to iterate through all positions in an array based off of array and size parameter passed to it in order to find the highest variable
{
	int highest = 0;

	for (int i = 0; i < size; i++)
	{
		if (array[i] > highest)
		{
			highest = array[i];
			highestPos = i; // sets the position of the current iteration to our global highestPos variable
		}
	}

	return highest;
}

int getLowest(int array[], int size) // method to iterate through all positions in an array based off of array and size parameter passed to it in order to find the lowest variable
{
	int lowest = array[0];

	for (int i = 0; i < size; i++)
	{
		if (array[i] < lowest)
		{
			lowest = array[i];
			lowestPos = i; // sets the position of the current iteration to our global lowestPos variable
		}
	}

	return lowest;
}

void outputSalsa(string name, int jarPos) // outputs a string with the name of the salsa type and the number of jars sold based off of the name and number sold input via parameters
{
	cout << "The total jars sold for the Salsa type \"" << name << "\"" << " is: " << jarPos << " jars\n";
}

void outputTotalHighLow(string arrayNames[], int jarsSold[]) // outputs the total number of jars, highest selling type, and lowest selling type of salsa
{
	cout << "The total amount of jars sold was: " << totalJarsSold << endl;
	cout << "The highest selling Salsa type was: \"" << arrayNames[highestPos] << "\" with " << getHighest(jarsSold, 5) << " jars sold.\n";
	cout << "The lowest selling Salsa type was: \"" << arrayNames[lowestPos] << "\" with " << getLowest(jarsSold, 5) << " jars sold.\n";
}

void output() // run all the individual output methods with correct parameters
{
	outputSalsa(salsaNames[0], jarsSold[0]);
	outputSalsa(salsaNames[1], jarsSold[1]);
	outputSalsa(salsaNames[2], jarsSold[2]);
	outputSalsa(salsaNames[3], jarsSold[3]);
	outputSalsa(salsaNames[4], jarsSold[4]);
	outputTotalHighLow(salsaNames, jarsSold);
}



int main() // runs all our code
{
	getInput(); // runs the getInput method to get user input
	while (checkInput(jarsSold, 5) == false) // input validation to continue running getInput until valid numbers are input
	{
		cout << "Input invalid. Please input your jars sold again.\n";
		getInput();
	}
	if (checkInput(jarsSold, 5) == true) // when valid input is entered, run the output method
	{
		output();
	}

	return 0;
}