// Drivers License Exam - ProLab9 Program 10.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Project Lab Lesson 9 - Program 10

#include "pch.h"
#include <iostream>
#include <string>
#include <algorithm>

using namespace std;


string correctAnswers[] = { "A", "D", "B", "B", "C", "B", "A", "B", "C", "D", "A", "C", "D", "B", "D", "C", "C", "A", "D", "B" }; // holds all our correct answer values as strings
string studentAnswers[20];
int incorrectAnswersNumbers[20];
int correctNumber, incorrectNumber;

void getInput() // outputs a cout prompting the user for input for each answer and stores their answer in an array
{
	for (int i = 0; i < 20; i++)
	{
		cout << "Input the correct answer for question number " << i + 1 << ":\n";
		cin >> studentAnswers[i];
	}
}

bool checkInput(string array[], int size) // validates input by checking to make sure the input is an acceptable answer (A, B, C, or D)
{
	for (int i = 0; i < size; i++)
	{
		if (array[i] == "A")
			return true;
		if (array[i] == "B")
			return true;
		if (array[i] == "C")
			return true;
		if (array[i] == "D")
			return true;
		else
			return false;
	}
}

void checkPassFail(string arrayStudent[], string arrayCorrect[])
{
	for (int i = 0; i < 20; i++)
	{
		if (arrayStudent[i] == arrayCorrect[i])
			correctNumber += 1;
		else
		{
			incorrectNumber += 1;
			incorrectAnswersNumbers[incorrectNumber - 1] = i;
		}
	}
}

void listIncorrect(int incorrectAnswers[], int size)
{
	for (int i = 0; i < size; i++)
		cout << "Incorrect answer on question number " << incorrectAnswers[i] + 1 << endl;
}



void output() // run all the individual output methods with correct parameters
{
	if (correctNumber >= 15)
	{
		cout << "You have passed with " << incorrectNumber << " incorrect answers and " << correctNumber << " correct answers.\n";
		listIncorrect(incorrectAnswersNumbers, incorrectNumber);
	}
	else
	{
		cout << "You have failed with " << incorrectNumber << " incorrect answers and " << correctNumber << " correct answers.\n";
		listIncorrect(incorrectAnswersNumbers, incorrectNumber);
	}
}



int main() // runs all our code
{
	getInput(); // runs the getInput method to get user input
	while (checkInput(studentAnswers, 20) == false) // input validation to continue running getInput until valid answers are input
	{
		cout << "Input invalid. Please input your answers again.\n";
		getInput();
	}
	if (checkInput(correctAnswers, 20) == true) // when valid input is entered, run the output method
	{
		checkPassFail(studentAnswers, correctAnswers);
		output();
	}

	return 0;
}