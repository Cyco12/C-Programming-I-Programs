// Winning Division - ProLab7 Program 3.cpp : This file contains the 'main' function. Program execution begins and ends there.
// LabPro7 Program 3

#include "pch.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>

using namespace std;

double northeastSales, southeastSales, northwestSales, southwestSales, highestSales; // Declare all of our number variables for different division sales and the overall highest sale
string highestSalesName = ""; // Declare an empty string variable to store the name of the highest selling division.


double getSales(string division) // getSales() method outputs a cout statement asking the user for the quarterly sales figure of the division whose name is passed into the method
{
	double salesAmt;
	cout << "What is the " + division + " division's quarterly sales figure?" << endl;
	cin >> salesAmt; // gets sales amount
	if (salesAmt > 0) { return salesAmt; } // checks to see that the sales amount is greater than 0. if it is, the method returns the sales amount.
	else // if the sales amount isnt greater than 0, tell the user and re-run the method.
	{
		cout << "The sales amount must be higher than 0." << endl;
		getSales(division);
	}
}

void findHighest(double sales1, double sales2, double sales3, double sales4) // findHighest() method takes 4 sales values to be passed it. it compares them to find the highest and then outputs the highest selling division and the amount sold.
{
	highestSales = max({ sales1, sales2, sales3, sales4 }); // uses the max() method to check which one of our sales values is the highest then stores it in the variable highestSales
	// the next four lines check to see which sales value the highestSales value is equal to in order to set the highestSalesName variable to that division's name.
	if (highestSales == sales1) { highestSalesName = "Northeast"; }
	if (highestSales == sales2) { highestSalesName = "Southeast"; }
	if (highestSales == sales3) { highestSalesName = "Northwest"; }
	if (highestSales == sales4) { highestSalesName = "Southwest"; }

	// output to the user the highest grossing division name and its sales amount.
	cout << "The highest grossing division was the " + highestSalesName + " division." << endl;
	cout << "The " + highestSalesName + " division made " + "$" << highestSales << endl;

	return;
}

int main()
{
	// runs all the code necessary to complete the desired tasks using our two functions defined above.
	northeastSales = getSales("Northeast");
	southeastSales = getSales("Southeast");
	northwestSales = getSales("Northwest");
	southwestSales = getSales("Southwest");
	findHighest(northeastSales, southeastSales, northwestSales, southwestSales);

	return 0;
}