// Safest Driving Area - ProLab7 Program 4.cpp : This file contains the 'main' function. Program execution begins and ends there.
// LabPro7 Program 4

#include "pch.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>

using namespace std;

double northAccidents, southAccidents, eastAccidents, westAccidents, centralAccidents, lowestAccidents; // Declare all of our number variables for different region accidents and the overall lowest amount of accidents
string lowestAccidentsName = ""; // Declare an empty string variable to store the name of the lowest accident prone area.


int getNumAccidents(string region) // getNumAccidents) method outputs a cout statement asking the user for the accidents last year of the region whose name is passed into the method
{
	double accidentAmt;
	cout << "What was the " + region + " region's number of accidents last year?" << endl;
	cin >> accidentAmt; // gets amount of accidents
	if (accidentAmt > 0) { return accidentAmt; } // checks to see that the sales amount is greater than 0. if it is, the method returns the sales amount.
	else // if the accidents amount isnt greater than 0, tell the user and re-run the method.
	{
		cout << "The sales amount must be higher than 0." << endl;
		getNumAccidents(region);
	}
}

void findLowest(int accidents1, int accidents2, int accidents3, int accidents4, int accidents5) // findLowest() method takes 5 accidents values that are passed to it. it compares them to find the lowest and then outputs the lowest accident amount region and the amount of accidents in that region.
{
	lowestAccidents = min({ accidents1, accidents2, accidents3, accidents4, accidents5 }); // uses the min() method to check which one of our region values is the lowest then stores it in the variable lowestAccidents
	// the next five lines check to see which accidents value the lowestAccidents value is equal to in order to set the lowestAccidentsName variable to that region's name.
	if (lowestAccidents == accidents1) { lowestAccidentsName = "North"; }
	if (lowestAccidents == accidents2) { lowestAccidentsName = "South"; }
	if (lowestAccidents == accidents3) { lowestAccidentsName = "East"; }
	if (lowestAccidents == accidents4) { lowestAccidentsName = "West"; }
	if (lowestAccidents == accidents5) { lowestAccidentsName = "Central"; }

	// output to the user the highest grossing division name and its sales amount.
	cout << "The lowest accident region was the " + lowestAccidentsName + " region." << endl;
	cout << "The " + lowestAccidentsName + " region had " << lowestAccidents << endl;

	return;
}

int main()
{
	// runs all the code necessary to complete the desired tasks using our two functions defined above.
	northAccidents = getNumAccidents("North");
	southAccidents = getNumAccidents("South");
	eastAccidents = getNumAccidents("East");
	westAccidents = getNumAccidents("West");
	centralAccidents = getNumAccidents("CEntral");
	findLowest(northAccidents, southAccidents, eastAccidents, westAccidents, centralAccidents);

	return 0;
}