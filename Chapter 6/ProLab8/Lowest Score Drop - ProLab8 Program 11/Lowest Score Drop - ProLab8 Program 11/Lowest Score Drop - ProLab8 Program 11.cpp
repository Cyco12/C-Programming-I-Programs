// Lowest Score Drop - ProLab8 Program 11.cpp : This file contains the 'main' function. Program execution begins and ends there.
// LabPro8 Programming 11

#include "pch.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>

using namespace std;

int score1, score2, score3, score4, score5, scoreAvg, droppedScore;


int getScore() // getScore() method outputs a cout statement asking the user for a test score 
{
	int inputScore;
	cout << "Input a test score: " << endl;
	cin >> inputScore; // gets test score
	if (inputScore >= 0 && inputScore <= 100) { return inputScore; } // checks to see that the test score is between 0 and 100. if it is, the method returns the test score.
	else // if the test score  isnt between 0 and 100, tell the user and re-run the method.
	{
		cout << "The sales amount must be higher than 0." << endl;
		getScore();
	}
}


int findLowest(int s1, int s2, int s3, int s4, int s5) // findLowest() method takes 5 score values that are passed to it. it compares them to find the lowest and then outputs the lowest score.
{
	int scoreLowest = min({ s1, s2, s3, s4, s5 }); // uses the min() method to check which one of our scoresis the lowest then stores it in the variable scoreLowest
	return scoreLowest;
}

void calcAverage(int s1, int s2, int s3, int s4, int s5)
{
	droppedScore = findLowest(s1, s2, s3, s4, s5);

	if (s1 == droppedScore) { scoreAvg = (s2 + s3 + s4 + s5) / 4; }
	if (s2 == droppedScore) { scoreAvg = (s1 + s3 + s4 + s5) / 4; }
	if (s3 == droppedScore) { scoreAvg = (s1 + s2 + s4 + s5) / 4; }
	if (s4 == droppedScore) { scoreAvg = (s1 + s2 + s3 + s5) / 4; }
	if (s5 == droppedScore) { scoreAvg = (s1 + s2 + s3 + s4) / 4; }


	cout << "The average of the four highest test scores is " << scoreAvg << endl;
}

int main()
{
	// runs all the code necessary to complete the desired tasks using our two functions defined above.
	score1 = getScore();
	score2 = getScore();
	score3 = getScore();
	score4 = getScore();
	score5 = getScore();
	calcAverage(score1, score2, score3, score4, score5);

	return 0;
}