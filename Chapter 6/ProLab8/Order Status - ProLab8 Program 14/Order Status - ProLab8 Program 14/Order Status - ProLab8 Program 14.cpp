// Order Status - ProLab8 Program 14.cpp : This file contains the 'main' function. Program execution begins and ends there.
// LabPro8 Program 14

#include "pch.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>

using namespace std;

const float perSpoolPrice = 100.00;
int spoolsOrdered, spoolsInStock, orderSpoolsReady, orderSpoolsBackorder;
float specialChargesPerSpool, orderSpoolsReadySubtotal, orderSpoolsReadySpecialCharges, orderSpoolsReadyTotal;
char shippingAndHandlingInput;
bool backOrder = false;


void getInput() // gets various user input and stores it
{
	// gets the count of spools ordered
	cout << "How many spools of copper wire have been ordered? " << endl;
	cin >> spoolsOrdered;
	if (spoolsOrdered < 1)
	{
		cout << "Invalid number of spools to order entered. Restarting input function." << endl;
		getInput();
	}
	// gets the count of spools that are in stock
	cout << "How many spools of copper wire are in stock? " << endl;
	cin >> spoolsInStock;
	if (spoolsInStock < 0)
	{
		cout << "Invalid number of spools in stock entered. Restarting input function." << endl;
		getInput();
	}
	// checks to see if there are any special s&h charges
	cout << "Are there any special shipping and handling charges? (Y/N) " << endl;
	cin >> shippingAndHandlingInput;

	// if there are special s&h charges, ask for what they are
	if (toupper(shippingAndHandlingInput) == 'Y')
	{
		cout << "Please enter the special shipping and handling charges per spool. " << endl;
		cin >> specialChargesPerSpool;
	}
	// if there arent any special s&h charges, return and end the function
	else if (toupper(shippingAndHandlingInput) == 'N')
	{
		return;
	}
	// if the input is invalid, tell the user
	else
	{
		cout << "Invalid input. " << endl;
	}
	return;
}

int calculate(int ordered, int stock, float extraCharges = 10.00) // takes in parameters for the number of spools ordered, number of spools in stock, and an optional parameter for the shipping and handling charges. If there is no s&h parameter passed to this functon, the default value is $10.00
{
	// checks to see if we ordered more spools than we have in stock
	if (ordered > stock)
	{
		orderSpoolsReady = stock;
		orderSpoolsBackorder = ordered - stock;
		backOrder = true;
	}
	// checks to see if we ordered less spools or an equal amount of spools compared to our number of spools in stock
	if (stock >= ordered)
	{
		orderSpoolsReady = ordered;
	}

	orderSpoolsReadySubtotal = orderSpoolsReady * perSpoolPrice; // calculates the subtotal based off the number of spools ready for order and the price per spool
	orderSpoolsReadySpecialCharges = extraCharges * orderSpoolsReady; // calculates and shipping and handling charges on all ready for order spools
	orderSpoolsReadyTotal = orderSpoolsReadySubtotal + orderSpoolsReadySpecialCharges; // calculates the total of the order based off of the subtotal and any special charges

	// outputs information to the user about the number of spools ordered, and the number of spools ready to order
	cout << "The number of spools ordered is: " << ordered << endl;
	cout << "The number of spools currently ready to ship is: " << orderSpoolsReady << endl;
	if (backOrder == true) // checks to see if there are any backorders
	{
		cout << "The backordered number of spools is: " << orderSpoolsBackorder << endl;
	}
	// outputs information to the user about the subtotal, s&h, and total costs of the order
	cout << "The subtotal of the order before shipping and handling charges is: $" << orderSpoolsReadySubtotal << endl;
	cout << "The total shipping and handling charges are: $" << orderSpoolsReadySpecialCharges << endl;
	cout << "The total cost of the order is: $" << orderSpoolsReadyTotal << endl;

	return 0;
}


int main()
{
	// runs all the code necessary to complete the desired tasks using our two functions defined above.
	getInput();

	// checks to see if we have any user input for special charges like shipping and handling
	if (specialChargesPerSpool != NULL)
	{
		calculate(spoolsOrdered, spoolsInStock, specialChargesPerSpool);
	}
	else
	{
		calculate(spoolsOrdered, spoolsInStock);
	}

	return 0;
}