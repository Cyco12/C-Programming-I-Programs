// Payroll Report - ProLab6 Program 15.cpp : This file contains the 'main' function. Program execution begins and ends there.
// ProLab 6 Program 15
/*15. Payroll Report
Write a program that displays a weekly payroll report. A loop in the program should ask the
user for the employee number, gross pay, state tax, federal tax, and FICA withholdings. The
loop will terminate when 0 is entered for the employee number. After the
data is entered, the program should display totals for gross pay, state tax, federal tax,
FICA withholdings, and net pay.

Input Validation: Do not accept negative numbers for any of the items entered. Do not
accept values for state, federal, or FICA with
holdings that are greater than the gross pay. If the
sum state tax + federal tax + FICA withholdings for any employee is greater than gross pay, print
an error message and ask the user to reenter the data for that employee*/

#include "pch.h"
#include <iostream>

using namespace std;

int main()
{
	int employeeNumber = 1;
	float grossPay, stateTax, federalTax, fciaWitholdings, netPay = 0, dividends;

	while (employeeNumber != 0)
	{
		cout << "Enter the employee number: " << endl;
		cin >> employeeNumber;
		if (employeeNumber == 0)
		{
			return 0;
		}
		cout << "Enter the employee's gross pay: " << endl;
		cin >> grossPay;
		if (grossPay < 0)
		{
			cout << "The gross pay cannot be below 0." << endl;
			return 0;
		}
		cout << "Enter the state tax: " << endl;
		cin >> stateTax;
		if (stateTax < 0)
		{
			cout << "The state tax cannot be below 0." << endl;
			return 0;
		}
		cout << "Enter the federal tax: " << endl;
		cin >> federalTax;
		if (federalTax < 0)
		{
			cout << "The federal tax cannot be below 0." << endl;
			return 0;
		}
		cout << "Enter the FCIA witholdings: " << endl;
		cin >> fciaWitholdings;
		if (fciaWitholdings < 0)
		{
			cout << "The FCIA withholdings cannot be below 0." << endl;
			return 0;
		}
		dividends = stateTax + federalTax + fciaWitholdings;
		netPay = grossPay - dividends;
		if (dividends > netPay)
		{
			cout << "Error. Please enter the employee's information again." << endl;
		}
		else
		{
			cout << "Employee number: " << employeeNumber << endl;
			cout << "Gross pay: " << grossPay << endl;
			cout << "State tax: " << stateTax << endl;
			cout << "Federal tax: " << federalTax << endl;
			cout << "FCIA Witholdings: " << fciaWitholdings << endl;
			cout << "Net pay: " << netPay << endl;
		}
	}

	return 0;
}
