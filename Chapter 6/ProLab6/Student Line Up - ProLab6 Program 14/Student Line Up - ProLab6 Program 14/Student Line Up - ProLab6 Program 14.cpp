// Student Line Up - ProLab6 Program 14.cpp : This file contains the 'main' function. Program execution begins and ends there.
// ProLab 6 Program 14
/*14. Student Line Up
A teacher has asked all her students to line up single file according to their first name. For
example, in one class Amy will be at the front of the line and Yolanda will be at the end.
Write a program that prompts the user to enter the number of students in the class, then loops to read
that many names. Once all the names have been read it reports which student would be at the
front of the line and which one would be at the end of the line. You may assume that no two
students have the same name.
Input Validation: Do not accept a number less than 1 or greater than 25 for the number of students.*/


#include "pch.h"
#include <iostream>
#include <string>
#include <set>
#include <algorithm>

using namespace std;

void print(const string& item)
{
	cout << item << endl;
}

int main()
{
	int studentCount, studentEvalCount;
	string studentName;
	cout << "How many students are there: " << endl;
	cin >> studentCount;
	if (studentCount >= 1 && studentCount <= 25)
	{
		set<string> studentNames;
		studentEvalCount = studentCount;
		while (studentEvalCount > 0)
		{
			cout << "Input student first name: " << endl;
			cin >> studentName;
			const bool is_in = studentNames.find(studentName) != studentNames.end();
			if (is_in == false)
			{
				studentNames.insert(studentName);
			}
			else
			{
				cout << "Student names can not be duplicates." << endl;
			}

			studentEvalCount--;
		}
		cout << endl << "The students in alphabetical order from first in line to last in line are:" << endl;
		for_each(studentNames.begin(), studentNames.end(), &print);
	}

	else
	{
		cout << "Student count must be between 1 and 25." << endl;
	}


	return 0;
}