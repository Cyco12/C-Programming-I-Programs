# C-Programming-I-Programs
These are programs I created for the C++ Programming I class I took at Gwinnett Technical College. 
They're all to complete excercises from the book 'C++ From Control Structures through Objects' Ninth Addition.

My goal is to eventually use these same concepts/excercises and create similar programs in other languages. 
This will be helpful for future comparison and examples (and also to further my knowledge in other languages).
